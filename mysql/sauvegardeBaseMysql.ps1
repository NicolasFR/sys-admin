###################################
# Sauvegarde d'une base de donnée #
# Mysql à l'aide de mysqldump     #
# version : 1.0.0                 #
# 17/12/2016                      #
###################################

# liste des variables à configurer
$mysqlBinPath = ""
$DB_name =""
$DB_user = ""
$DB_pass =""
$log="log.log"
$pathSauvegarde=""


# création du nom du fichier
$date = Get-Date
$horodatage = "" + $date.day +"-"+ $date.month +"-"+ $date.year + "_" + $date.hour +"h"+ $date.minute
$fichierSauvegarde = $pathSauvegarde + $DB_name +"_"+ $horodatage + ".sql"

CD $mysqlBinPath
#éxecute mysqldump
.\mysqldump.exe --user=$DB_user --password=$DB_pass --log-error=$log --result-file=$fichierSauvegarde --databases $DB_name
echo.
echo Sauvegarde terminé

type $log
pause