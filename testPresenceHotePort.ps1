﻿############################################################
# Test présence d'un hôte et si port spécifique est ouvert #
# 21/12/2016                                               #
# 1.0.0 Nicolas Ferrier                                    #
############################################################

$hotes = '192.168.1.1','Server2','Server3','Server4'
$portAVerifier = '80'

foreach ($hote in $hotes) {

    If ( Test-Connection $hote -Count 1 -Quiet) {
    
        try {       
            $null = New-Object System.Net.Sockets.TCPClient -ArgumentList $hote,$portAVerifier
            $props = @{
                Hôte = $hote
                PortOuvert = 'Oui'
            }
        }

        catch {
            $props = @{
                Hôte = $hote
                PortOuvert = 'Non'
            }
        }
    }

    Else {
        
        $props = @{
            Hôte = $hote
            PortOuvert = 'Le serveur ne répond pas au ping'
        }
    }

    New-Object PsObject -Property $props

}