﻿$InputDirectory = "F:\@E4\draft\Situation2"
$OutputDirectory = "F:\@E4\draft\pdf"
$LibreOfficeExe = "C:\Program Files\LibreOffice\program\soffice.exe"
$ConvertTo = "pdf"
$ConvertFrom = "docx"


Write-Verbose "Checking if LibreOffice EXE Location exists"
If (!(Test-Path $LibreOfficeExe))
{
    Write-Error "LibreOffice EXE location was not found, please specify another location"
}

Write-Verbose "Checking if OutputDirectory exists"
if (!(Test-Path $OutputDirectory))
{
    New-Item -ItemType Directory $Out
}

Write-Verbose "Checking if InputDirectory exists"
if (!(Test-Path $InputDirectory))
{
    Write-Error "Input directory was not found, please speciy another location"
}

$Files = Get-ChildItem $InputDirectory -Filter "*.$ConvertFrom"

foreach($File in $Files)
{
    if ($File.Exists)
    {
        $Argument = '--headless --convert-to ' + $ConvertTo + ' --outdir "' + $OutputDirectory + '" "' + $File.FullName + '"'
        Write-Verbose "Starting convert using Arguments: $Argument"
        Start-Process $LibreOfficeExe -ArgumentList $Argument -Wait
        Write-Host "$File has been converted"
    }
}

Write-Host "Script has been completed"